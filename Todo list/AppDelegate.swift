//
//  AppDelegate.swift
//  Todo list
//
//  Created by Chan Ka Kui on 11/2/2020.
//  Copyright © 2020 awcjack. All rights reserved.
//

import Cocoa

var statusbar = NSStatusBar.system
var statusbaritem: NSStatusItem!
let statusmenu = NSMenu(title: "ToDoList")
let filemanager = FileManager.default
let url = filemanager.urls(for: .documentDirectory, in: .userDomainMask)[0] as URL
let folder = url.appendingPathComponent("ToDo", isDirectory: true)
var file = folder.appendingPathComponent("data", isDirectory: false)

struct ToDoItem {
    var name: String
    var works: Array<String>
}

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    func applicationDidFinishLaunching(_ notification: Notification) {
        initize()
    }
    //Init
    func initize(){
        //Check data existence
        confirmDataExist()
        statusbaritem = statusbar.statusItem(withLength: NSStatusItem.squareLength)
        statusbaritem.button?.title = "🗓"
        statusbaritem.menu = statusmenu
        //Add todo button
        mustExistItem(statusmenu: statusmenu)
        //Load from file (must exist after confirmDataExist())
        
        
        //Quit button
        statusmenu.addItem(withTitle: "Quit", action: #selector(NSApplication.shared.terminate(_:)), keyEquivalent: "Q")
    }
    //Create data file if not exist
    func confirmDataExist(){
        if !filemanager.fileExists(atPath: folder.path) {
            //data folder not exist -> create folder
            try? filemanager.createDirectory(at: folder, withIntermediateDirectories: true, attributes: nil)
        }
        file = file.appendingPathExtension("json")
        if !filemanager.fileExists(atPath: file.path){
            //data file not exist -> create file without content
            filemanager.createFile(atPath: file.path, contents: nil, attributes: nil)
        }
    }
    //ToDo Button
    func mustExistItem(statusmenu: NSMenu){
        var newToDoButton = NSMenuItem(title: "Add New ToDo item", action: #selector(self.AddNewToDo(_:)), keyEquivalent: "")
        //newToDoButton.attributedTitle = NSAttributedString(string: "test1\ntest2");
        statusmenu.addItem(newToDoButton)
        statusmenu.addItem(NSMenuItem.separator())
        //statusmenu.addItem(withTitle: "Quit", action: #selector(NSApplication.shared.terminate(_:)), keyEquivalent: "")
    }
    //Add new todo item: pop up for project, subitem, deadline --> write to file --> trigger reload
    @objc func AddNewToDo(_ sender: Any?){
        let alert = NSAlert()
        alert.messageText = "Add todo work"
        alert.addButton(withTitle: "Add!")
        let textfield = NSTextField(frame: NSRect(x: 0, y: 30, width: 100, height: 20))
        textfield.placeholderString = "Project name"
        let textfield2 = NSTextField(frame: NSRect(x: 0, y: 0, width: 500, height: 20))
        textfield2.placeholderString = "Works (separated by ;)"
        let stackView = NSStackView(frame: NSRect(x: 0, y: 0, width: 500, height: 50))
        stackView.addSubview(textfield)
        stackView.addSubview(textfield2)
        textfield.nextKeyView = textfield2
        alert.accessoryView = stackView
        alert.runModal()
        let project = textfield.stringValue
        let works = textfield2.stringValue
        let workArray = works.components(separatedBy: ";")
        let trimmedWorkArray = workArray.map {$0.trimmingCharacters(in: .whitespacesAndNewlines)}
        let newToDo = ToDoItem(name: project, works: trimmedWorkArray)
    }
}

/*
@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    let statusitem = NSStatusBar.system.statusItem(withLength:NSStatusItem.squareLength)
    let popover = NSPopover()
    @objc func togglePopover(_ sender: Any?) {
      if popover.isShown {
        closePopover(sender: sender)
      } else {
        showPopover(sender: sender)
      }
    }
    func showPopover(sender: Any?) {
      if let button = statusitem.button {
        popover.show(relativeTo: button.bounds, of: button, preferredEdge: NSRectEdge.minY)
      }
    }

    func closePopover(sender: Any?) {
      popover.performClose(sender)
    }
    
    func applicationDidFinishLaunching(_ notification: Notification) {
        if let button = statusitem.button {
          button.image = NSImage(named:NSImage.Name("StatusBarButtonImage"))
          button.action = #selector(togglePopover(_:))
        }
        //constructMenu()
        popover.contentViewController = ViewController.freshController()
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
}
*/
